//
//  PersonDetailViewController.m
//  InterviewProject
//
//  Created by Rune Madsen on 2014-09-15.
//  Copyright (c) 2014 Milq. All rights reserved.
//

#import "PersonDetailViewController.h"

@interface PersonDetailViewController ()

@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UIButton *favouriteButton;

@end

@implementation PersonDetailViewController

-(void)setPerson:(Person *)person {
    _person = person;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.nameLabel.attributedText = self.person.attributedString;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
