//
//  Extras.h
//  InterviewProject
//
//  Created by Rune Madsen on 2014-09-15.
//  Copyright (c) 2014 Milq. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Extras : NSObject







+(NSArray*)aFewPeople;
+(NSArray*)lotsOfPeople;

















































































+(void)arrayWithNumberOfPeople:(NSUInteger)count completion:(void (^)(NSArray* people))completion;















































+(void)arrayWithNumberOfPersons:(NSUInteger)count completion:(void (^)(NSArray* people))completion;

+(NSArray*)favouriteFirstNames;

@end
