//
//  TableViewControllerThree.m
//  InterviewProject
//
//  Created by Rune Madsen on 2014-09-15.
//  Copyright (c) 2014 Milq. All rights reserved.
//

// fix checkmark bug
// improve check for favourite
// improve storing of favourites - compare string, compare object
// images, how do you load them without blocking UI?
// update favourite state in detail

#import "TableViewControllerThree.h"
#import "Extras.h"
#import "Person.h"
#import "PersonDetailViewController.h"

@interface TableViewControllerThree ()

@property (nonatomic, strong) NSArray *arrayOfPeople;
@property (nonatomic, strong) NSArray *favouriteFirstNames;

@end

@implementation TableViewControllerThree

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.favouriteFirstNames = [Extras favouriteFirstNames];
    
    [Extras arrayWithNumberOfPersons:50
                         completion:^(NSArray *people) {
                             self.arrayOfPeople = people;
                             [self.tableView reloadData];
                         }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [self.arrayOfPeople count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    Person *person = self.arrayOfPeople[indexPath.row];
    
    cell.textLabel.attributedText = person.attributedString;
    
    for (NSString *name in self.favouriteFirstNames) {
        if ([name isEqualToString:person.firstName]) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
    }
    
    return cell;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    Person *person = self.arrayOfPeople[indexPath.row];
    PersonDetailViewController *personDetailViewController = [segue destinationViewController];
    [personDetailViewController setPerson:person];
}

@end
