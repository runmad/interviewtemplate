//
//  TableViewControllerTwo.m
//  InterviewProject
//
//  Created by Rune Madsen on 2014-09-15.
//  Copyright (c) 2014 Milq. All rights reserved.
//

// add loading indicator
// sort by first name
// bold last name

#import "TableViewControllerTwo.h"
#import "Extras.h"

@interface TableViewControllerTwo ()

@property (nonatomic, strong) NSArray *arrayOfPeople;

@end

@implementation TableViewControllerTwo

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [Extras arrayWithNumberOfPeople:999999
                         completion:^(NSArray *people) {
                             self.arrayOfPeople = people;
                             [self.tableView reloadData];
                         }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [self.arrayOfPeople count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    cell.textLabel.text = self.arrayOfPeople[indexPath.row];
    
    return cell;
}

@end
