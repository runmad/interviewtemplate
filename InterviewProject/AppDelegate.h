//
//  AppDelegate.h
//  InterviewProject
//
//  Created by Rune Madsen on 2014-09-15.
//  Copyright (c) 2014 Milq. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
