//
//  Person.m
//  InterviewProject
//
//  Created by Rune Madsen on 2014-09-15.
//  Copyright (c) 2014 Milq. All rights reserved.
//

#import "Person.h"
#import <UIKit/UIKit.h>

@implementation Person

-(NSString*)fullName {
    return [NSString stringWithFormat:@"%@ %@", self.firstName, self.lastName];
}

-(void)setupName {
    UIFont *font = [UIFont preferredFontForTextStyle:UIFontTextStyleBody];
    NSMutableAttributedString *mutableAttributedString = [[NSMutableAttributedString alloc] initWithString:self.fullName];
    [mutableAttributedString addAttribute:NSFontAttributeName value:font range:NSMakeRange(0, [mutableAttributedString length])];
    
    UIFontDescriptor *fontDescriptor = [UIFontDescriptor preferredFontDescriptorWithTextStyle:UIFontTextStyleBody];
    UIFontDescriptor *symbolicFontDescriptor = [fontDescriptor fontDescriptorWithSymbolicTraits:UIFontDescriptorTraitBold];
    UIFont *boldFont = [UIFont fontWithDescriptor:symbolicFontDescriptor size:font.pointSize];
    
    [mutableAttributedString addAttribute:NSFontAttributeName value:boldFont range:NSMakeRange([self.firstName length] + 1, [self.lastName length])];
    self.attributedString = [[NSAttributedString alloc] initWithAttributedString:[mutableAttributedString copy]];
}

@end
